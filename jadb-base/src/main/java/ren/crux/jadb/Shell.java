/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb;

import ren.crux.jadb.base.AndroidDebugBridgeBase;
import ren.crux.jadb.base.ShellBase;

/**
 * @author wangzhihui
 */
public class Shell {

    private final ShellBase base;

    public final ActivityManager am;
    public final PackageManager pm;
    public final Input input;

    public Shell(AndroidDebugBridgeBase base) {
        this.base = new ShellBase(base);
        this.am = new ActivityManager(this.base);
        this.pm = new PackageManager(this.base);
        this.input = new Input(this.base);
    }
}
