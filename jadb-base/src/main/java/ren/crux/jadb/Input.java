/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb;

import org.apache.commons.lang3.StringUtils;
import ren.crux.jadb.base.InputBase;
import ren.crux.jadb.base.ShellBase;
import ren.crux.jadb.model.KeyEvent;
import ren.crux.jadb.model.Target;

/**
 * @author wangzhihi
 */
public class Input {

    protected final InputBase base;

    public Input(ShellBase base) {
        this.base = new InputBase(base);
    }

    /**
     * Inputs key event.
     *
     * @param target
     * @param keyEvent
     * @return
     * @throws Exception
     */
    public boolean keyevent(Target target, KeyEvent keyEvent) throws Exception {
        String output = base.keyevent(target, keyEvent);
        return StringUtils.isBlank(output);
    }

    public boolean keyevent(KeyEvent keyEvent) throws Exception {
        return keyevent(null, keyEvent);
    }

    /**
     * Inputs text.
     *
     * @param target
     * @param content
     * @return
     * @throws Exception
     */
    public boolean text(Target target, String content) throws Exception {
        String output = base.text(target, content);
        return StringUtils.isBlank(output);
    }

    public boolean text(String content) throws Exception {
        return text(null, content);
    }

    /**
     * Inputs a tap event at the specific coordinates in pixels.
     *
     * @param target
     * @param x
     * @param y
     * @return
     * @throws Exception
     */
    public boolean tap(Target target, int x, int y) throws Exception {
        String output = base.tap(target, x, y);
        return StringUtils.isBlank(output);
    }

    public boolean tap(int x, int y) throws Exception {
        return tap(null, x, y);
    }

    /**
     * Inputs a swipe gesture starting at a set of coordinates, ending at another set of coordinates.
     *
     * @param target
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     * @throws Exception
     */
    public boolean swipe(Target target, int x1, int y1, int x2, int y2) throws Exception {
        String output = base.swipe(target, x1, y1, x2, y2);
        return StringUtils.isBlank(output);
    }

    public boolean swipe(int x1, int y1, int x2, int y2) throws Exception {
        return swipe(null, x1, y1, x2, y2);
    }

}
