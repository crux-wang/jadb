/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb;

/**
 * @author wangzhihui
 */
public interface Consts {

    String BASH = "bash";
    String ADB = "adb";

    interface Command {

        String DEVICES = "devices";
        String CONNECT = "connect";
        String DISCONNECT = "disconnect";
        String INSTALL = "install";
        String UNINSTALL = "uninstall";
        String START_SERVER = "start-server";
        String KILL_SERVER = "kill-server";
        String version = "version";
        String SHELL = "shell";
        String TCPIP = "tcpip";
        String USB = "usb";

        interface Device {
            String LIST = "-l";
        }

        interface Shell {

            String ACTIVITY_MANAGER = "am";
            String PACKAGE_MANAGER = "pm";
            String INPUT = "input";

            interface PackageManager {

                String LIST = "list";
                String GRANT = "grant";
                String REVOKE = "revoke";
                String CLEAR = "clear";

                interface List {
                    String PACKAGES = "packages";
                    String PERMISSION_GROUPS = "permission-groups";
                    String PERMISSIONS = "permissions";
                }
            }

            interface ActivityManager {
                String FORCE_STOP = "force-stop";
                String KILL = "kill";
                String KILL_ALL = "kill-all";
                String START = "start";
                String START_SERVICE = "startservice";
                String BROADCAST = "broadcast";
                String TO_URI = "to-uri";
                String TO_INTENT_URI = "to-intent-uri";
            }

            interface Input {
                String KEY_EVENT = "keyevent";
                String TEXT = "text";
                String TAP = "tap";
                String SWIPE = "swipe";
            }
        }
    }

    interface Option {

        interface Global {
            String SERIAL_NUMBER = "-s";
            String DEVICE = "-d";
            String EMULATOR = "-e";
        }

        interface App {

            interface Install {
                String REPLACE = "-r";
                String GRANT_ALL = "-g";
            }

            interface Uninstall {
                String KEEP = "-k";
            }

        }

        interface ActivityManager {

            String WAIT = "-W";
            String STOP = "-S";
            String REPEAT = "-R";
            String DEBUG = "-D";

            interface Intent {
                String ACTION = "-a";
                String DATA_URI = "-d";
                String CATEGORY = "-c";
                String COMPONENT = "-n";
                String MIME_TYPE = "-t";
                String FLAGS = "-f";
                String EXTRA_KEY = "--esn";
                String EXTRA_STRING = "--es";
                String EXTRA_BOOL = "--ez";
                String EXTRA_INT = "--ei";
                String EXTRA_LONG = "--el";
                String EXTRA_FLOAT = "--ef";
                String EXTRA_URI = "--eu";
                String EXTRA_COMPONENT_NAME = "--ecn";
                String EXTRA_INT_ARR = "--eia";
                String EXTRA_LONG_ARR = "--ela";
                String EXTRA_FLOAT_ARR = "--efa";
                String SELECTOR = "--selector";
            }
        }

        interface PackageManager {
            String SYSTEM = "-s";
            String THIRD_PARTY = "-3";
            String DANGEROUS = "-d";
        }
    }

    interface Output {
        String DEVICES_KEY_WORD = "List of devices attached";
        String CONNECT_KEY_WORD = "connected to ";
        String CONNECT_ALREADY_KEY_WORD = "already connected to ";
        String DISCONNECT_KEY_WORD = "disconnected ";
        String SUCC_KEY_WORD = "Success";
        String PACKAGE_PREFIX = "package:";
        String PERMISSION_GROUP_PREFIX = "permission group:";
        String PERMISSION_PREFIX = "permission:";
        String ALL_PERMISSION_KEY_WORD = "All Permissions:";
        String DANGEROUS_PERMISSION_KEY_WORD = "Dangerous Permissions:";
        String STOPPING_KEY_WORD = "Stopping:";
        String STARTING_KEY_WORD = "Starting:";
        String STATUS_OK_KEY_WORD = "Status: ok";
        String STARTING_SERVICE_KEY_WORD = "Starting service:";
        String BROADCASTING_KEY_WORD = "Broadcasting:";
        String TCPIP_KEY_WORD = "restarting in TCP mode port: ";
        String USB_KEY_WORD = "restarting in USB mode";
    }

}
