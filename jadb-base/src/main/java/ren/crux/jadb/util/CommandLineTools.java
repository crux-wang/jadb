/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;

/**
 * @author wangzhihui
 */
@Slf4j
public class CommandLineTools {

    public static long timeout = ExecuteWatchdog.INFINITE_TIMEOUT;

    public static String exec(final CommandLine command) throws Exception {
        return exec(command, timeout);
    }

    public static String exec(@NonNull final CommandLine command, long timeout) throws Exception {
        log.trace("Command : {} {}", command.getExecutable(), StringUtils.join(command.getArguments(), " "));
        final ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DefaultExecutor executor = new DefaultExecutor();
        executor.setWatchdog(watchdog);
        executor.setStreamHandler(new PumpStreamHandler(baos, baos));
        executor.execute(command);
        String output = baos.toString().trim();
        log.debug("Command : {} {}\nOutput :\n{}", command.getExecutable(), StringUtils.join(command.getArguments(), " "), output);
        return output;
    }

}
