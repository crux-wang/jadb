/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangzhihui
 */
public class OutputUtil {

    public static String requiredNonBlank(String output) throws Exception {
        if (StringUtils.isNotBlank(output)) {
            return output.trim();
        }
        throw new Exception("no-output.");
    }

    public static List<String> readLines(String output) throws Exception {
        List<String> rows = new ArrayList<>();
        if (StringUtils.isNotBlank(output)) {
            String[] strings = output.split("\n");
            for (String string : strings) {
                if (StringUtils.isNotBlank(string.trim())) {
                    rows.add(string.trim());
                }
            }
        }
        return rows;
    }
}
