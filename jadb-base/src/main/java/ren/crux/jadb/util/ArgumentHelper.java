/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.util;

import lombok.NonNull;
import org.apache.commons.exec.CommandLine;
import ren.crux.jadb.model.Target;
import ren.crux.jadb.model.TargetType;

/**
 * Command Argument Helper
 *
 * @author wangzhihui
 */
public class ArgumentHelper {

    public static CommandLine addTarget(@NonNull CommandLine command, Target target) {
        if (target != null) {
            TargetType targetType = target.getType();
            command.addArgument(targetType.getArg());
            if (targetType == TargetType.serial_number) {
                command.addArgument(target.getSerialNumber());
            }
        }
        return command;
    }
}
