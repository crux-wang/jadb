/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb;

import org.apache.commons.lang3.StringUtils;
import ren.crux.jadb.base.PackageManagerBase;
import ren.crux.jadb.base.ShellBase;
import ren.crux.jadb.model.Target;
import ren.crux.jadb.util.OutputUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangzhihui
 */
public class PackageManager {

    protected final PackageManagerBase base;

    public PackageManager(ShellBase base) {
        this.base = new PackageManagerBase(base);
    }

    /**
     * Prints all packages, optionally only those whose package name contains the text in filter.
     *
     * @param target
     * @param system
     * @param filter
     * @return
     * @throws Exception
     */
    public List<String> listPackages(Target target, Boolean system, String filter) throws Exception {
        String output = base.listPackages(target, system, filter);
        List<String> rows = OutputUtil.readLines(output);
        List<String> packages = new ArrayList<>();
        for (String row : rows) {
            packages.add(row.replaceFirst(Consts.Output.PACKAGE_PREFIX, ""));
        }
        return packages;
    }

    public List<String> listPackages(Boolean system, String filter) throws Exception {
        return listPackages(null, system, filter);
    }

    /**
     * Prints all known permission groups.
     *
     * @param target
     * @return
     * @throws Exception
     */
    public List<String> listPermissionGroups(Target target) throws Exception {
        String output = base.listPermissionGroups(target);
        List<String> rows = OutputUtil.readLines(output);
        List<String> groups = new ArrayList<>();
        for (String row : rows) {
            groups.add(row.replaceFirst(Consts.Output.PERMISSION_GROUP_PREFIX, ""));
        }
        return groups;
    }

    public List<String> listPermissionGroups() throws Exception {
        return listPermissionGroups(null);
    }

    /**
     * Prints all known permissions, optionally only those in group.
     *
     * @param target
     * @param dangerous
     * @return
     * @throws Exception
     */
    public List<String> listPermissions(Target target, boolean dangerous) throws Exception {
        String output = base.listPermissions(target, dangerous);
        List<String> rows = OutputUtil.readLines(output);
        List<String> permissions = new ArrayList<>();
        for (String row : rows) {
            if (StringUtils.equals(row, Consts.Output.ALL_PERMISSION_KEY_WORD) || StringUtils.equals(row, Consts.Output.DANGEROUS_PERMISSION_KEY_WORD)) {
                continue;
            }
            permissions.add(row.replaceFirst(Consts.Output.PERMISSION_PREFIX, ""));
        }
        return permissions;
    }

    public List<String> listPermissions(boolean dangerous) throws Exception {
        return listPermissions(null, dangerous);
    }

    /**
     * Grant a permission to an app.
     * <p>
     * On devices running Android 6.0 (API level 23) and higher, the permission can be any permission declared in the app manifest.
     * On devices running Android 5.1 (API level 22) and lower, must be an optional permission defined by the app.
     *
     * @param target
     * @param packageName
     * @param permission
     * @return
     * @throws Exception
     */
    public boolean grant(Target target, String packageName, String permission) throws Exception {
        String output = base.grant(target, packageName, permission);
        return StringUtils.isBlank(output);
    }

    public boolean grant(String packageName, String permission) throws Exception {
        return grant(null, packageName, permission);
    }

    /**
     * Revoke a permission from an app.
     * <p>
     * On devices running Android 6.0 (API level 23) and higher, the permission can be any permission declared in the app manifest.
     * On devices running Android 5.1 (API level 22) and lower, must be an optional permission defined by the app.
     *
     * @param target
     * @param packageName
     * @param permission
     * @return
     * @throws Exception
     */
    public boolean revoke(Target target, String packageName, String permission) throws Exception {
        String output = base.revoke(target, packageName, permission);
        return StringUtils.isBlank(output);
    }

    public boolean revoke(String packageName, String permission) throws Exception {
        return revoke(null, packageName, permission);
    }

    /**
     * Deletes all data associated with a package.
     *
     * @param target
     * @param packageName
     * @return
     * @throws Exception
     */
    public boolean clear(Target target, String packageName) throws Exception {
        String output = base.clear(target, packageName);
        return output.equals(Consts.Output.SUCC_KEY_WORD);
    }

    public boolean clear(String packageName) throws Exception {
        return clear(null, packageName);
    }
}
