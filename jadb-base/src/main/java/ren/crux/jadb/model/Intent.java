/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.model;

import lombok.Data;
import lombok.NonNull;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static ren.crux.jadb.Consts.Option.ActivityManager.Intent.*;

/**
 * @author wangzhihui
 */
@Data
public class Intent {

    private String action;
    private String dataUri;
    private String category;
    private String component;
    private Integer flagsValue;
    private String mimeType;
    private Map<String, Object> extras = new HashMap<>();
    private boolean selector = false;
    private List<Flags> flags = new ArrayList<>();


    public Object putString(@NonNull String key, @NonNull String value) {
        return extras.put(key, value);
    }

    public Object putBool(@NonNull String key, boolean value) {
        return extras.put(key, value);
    }

    public Object putInt(@NonNull String key, int value) {
        return extras.put(key, value);
    }

    public Object putLong(@NonNull String key, long value) {
        return extras.put(key, value);
    }

    public Object putFloat(@NonNull String key, float value) {
        return extras.put(key, value);
    }

    public Object putIntArray(@NonNull String key, int[] value) {
        return extras.put(key, value);
    }

    public Object putLongArray(@NonNull String key, long[] value) {
        return extras.put(key, value);
    }

    public Object putFloatArray(@NonNull String key, float[] value) {
        return extras.put(key, value);
    }

    public Object getExtra(@NonNull String key) {
        return extras.get(key);
    }

    public String getString(@NonNull String key, String def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof String) {
            return (String) val;
        }
        return def;
    }

    public Boolean getBool(@NonNull String key, Boolean def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof Boolean) {
            return (Boolean) val;
        }
        return def;
    }

    public String getInt(@NonNull String key, String def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof String) {
            return (String) val;
        }
        return def;
    }

    public Long getLong(@NonNull String key, Long def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof Long) {
            return (Long) val;
        }
        return def;
    }

    public Float getFloat(@NonNull String key, Float def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof Float) {
            return (Float) val;
        }
        return def;
    }

    public int[] getIntArray(@NonNull String key, int[] def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof int[]) {
            return (int[]) val;
        }
        return def;
    }

    public long[] getLongArray(@NonNull String key, long[] def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof long[]) {
            return (long[]) val;
        }
        return def;
    }

    public float[] getFloatArray(@NonNull String key, float[] def) {
        Object val = extras.getOrDefault(key, def);
        if (val instanceof float[]) {
            return (float[]) val;
        }
        return def;
    }

    public void addFlags(Flags... flags) {
        if (flags != null) {
            this.flags.addAll(Arrays.asList(flags));
        }
    }

    public CommandLine write(CommandLine command) {

        valid2AddArgs(command, action, ACTION);
        valid2AddArgs(command, dataUri, DATA_URI);
        valid2AddArgs(command, category, CATEGORY);
        valid2AddArgs(command, component, COMPONENT);
        valid2AddArgs(command, mimeType, MIME_TYPE);
        if (flagsValue != null) {
            command.addArgument(FLAGS).addArgument(String.valueOf(flags));
        } else {
            flags.forEach(flag -> command.addArgument(flag.getArg()));
        }
        if (selector) {
            command.addArgument(SELECTOR);
        }

        extras.forEach((key, value) -> {
            boolean valid = true;
            if (value instanceof String) {
                command.addArgument(EXTRA_STRING);
            } else if (value instanceof Boolean) {
                command.addArgument(EXTRA_BOOL);
            } else if (value instanceof Integer) {
                command.addArgument(EXTRA_INT);
            } else if (value instanceof Long) {
                command.addArgument(EXTRA_LONG);
            } else if (value instanceof Float) {
                command.addArgument(EXTRA_FLOAT);
            } else if (value instanceof int[]) {
                command.addArgument(EXTRA_INT_ARR);
            } else if (value instanceof long[]) {
                command.addArgument(EXTRA_LONG_ARR);
            } else if (value instanceof float[]) {
                command.addArgument(EXTRA_FLOAT_ARR);
            } else {
                valid = false;
            }
            if (valid) {
                command.addArgument(key);
                if (value instanceof int[] || value instanceof long[] || value instanceof float[]) {
                    command.addArgument(StringUtils.join(",", value));
                } else {
                    command.addArgument(String.valueOf(value));
                }
            }
        });

        return command;
    }

    private void valid2AddArgs(CommandLine command, String value, String arg) {
        if (StringUtils.isNotBlank(value)) {
            command.addArgument(arg).addArgument(value);
        }
    }
}
