/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.model;

import lombok.Data;
import lombok.NonNull;

/**
 * @author wangzhihui
 */
@Data
public class Target {

    private final TargetType type;
    private final String serialNumber;

    public Target(TargetType type, String serialNumber) {
        this.type = type;
        this.serialNumber = serialNumber;
    }

    public static Target device() {
        return new Target(TargetType.device, "");
    }

    public static Target emulator() {
        return new Target(TargetType.emulator, "");
    }

    public static Target device(@NonNull String serialNumber) {
        return new Target(TargetType.serial_number, serialNumber);
    }
}
