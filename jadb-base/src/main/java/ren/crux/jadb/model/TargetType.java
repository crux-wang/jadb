/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.model;

import static ren.crux.jadb.Consts.Option.Global.*;

/**
 * @author wangzhihui
 */

public enum TargetType {

    /**
     * Direct an adb command to the only attached USB device.
     * Returns an error when more than one USB device is attached.
     */
    device(DEVICE),
    /**
     * Direct an adb command to the only running emulator.
     * Returns an error when more than one emulator is running.
     */
    emulator(EMULATOR),
    /**
     * Direct an adb command to a specific device, referred to by its adb-assigned serial number (such as emulator-5556).
     * Overrides the serial number value stored in the $ANDROID_SERIAL environment variable.
     */
    serial_number(SERIAL_NUMBER);

    private final String arg;

    TargetType(String arg) {
        this.arg = arg;
    }

    public String getArg() {
        return arg;
    }
}
