/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author wangzhihui
 */
@Data
public class Device {

    private String serialNumber;
    private DeviceState state;
    private String usb;
    private String product;
    private String model;
    private String device;
    private int transportId;
    private boolean emulator;

    public boolean usb() {
        return StringUtils.isNotBlank(usb);
    }

}
