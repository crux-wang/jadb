/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.base;

import lombok.NonNull;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringUtils;
import ren.crux.jadb.Consts;
import ren.crux.jadb.model.Target;
import ren.crux.jadb.util.CommandLineTools;

import static ren.crux.jadb.Consts.Command.Shell.PackageManager.*;

/**
 * @author wangzhihui
 */
public class PackageManagerBase {

    protected final ShellBase base;

    public PackageManagerBase(ShellBase base) {
        this.base = base;
    }

    protected CommandLine create(Target target) {
        return base.create(target)
                .addArgument(Consts.Command.Shell.PACKAGE_MANAGER);
    }

    /**
     * Prints all packages, optionally only those whose package name contains the text in filter.
     *
     * @param target
     * @param system
     * @param filter
     * @return
     * @throws Exception
     */
    public String listPackages(Target target, Boolean system, String filter) throws Exception {
        CommandLine command = create(target)
                .addArgument(LIST)
                .addArgument(List.PACKAGES);
        if (system != null) {
            if (system) {
                command.addArgument(Consts.Option.PackageManager.SYSTEM);
            } else {
                command.addArgument(Consts.Option.PackageManager.THIRD_PARTY);
            }
        }
        if (StringUtils.isNotBlank(filter)) {
            command.addArgument(filter);
        }
        return CommandLineTools.exec(command);
    }

    public String listPackages(Boolean system, String filter) throws Exception {
        return listPackages(null, system, filter);
    }

    /**
     * Prints all known permission groups.
     *
     * @param target
     * @return
     * @throws Exception
     */
    public String listPermissionGroups(Target target) throws Exception {
        CommandLine command = create(target)
                .addArgument(LIST)
                .addArgument(List.PERMISSION_GROUPS);
        return CommandLineTools.exec(command);
    }

    public String listPermissionGroups() throws Exception {
        return listPermissionGroups(null);
    }

    /**
     * Prints all known permissions, optionally only those in group.
     *
     * @param target
     * @param dangerous
     * @return
     * @throws Exception
     */
    public String listPermissions(Target target, boolean dangerous) throws Exception {
        CommandLine command = create(target)
                .addArgument(LIST)
                .addArgument(List.PERMISSIONS);
        if (dangerous) {
            command.addArgument(Consts.Option.PackageManager.DANGEROUS);
        }
        return CommandLineTools.exec(command);
    }

    public String listPermissions(boolean dangerous) throws Exception {
        return listPermissions(null, dangerous);
    }

    /**
     * Grant a permission to an app.
     * <p>
     * On devices running Android 6.0 (API level 23) and higher, the permission can be any permission declared in the app manifest.
     * On devices running Android 5.1 (API level 22) and lower, must be an optional permission defined by the app.
     *
     * @param target
     * @param packageName
     * @param permission
     * @return
     * @throws Exception
     */
    public String grant(Target target, @NonNull String packageName, @NonNull String permission) throws Exception {
        CommandLine command = create(target)
                .addArgument(GRANT)
                .addArgument(packageName)
                .addArgument(permission);
        return CommandLineTools.exec(command);
    }

    public String grant(@NonNull String packageName, @NonNull String permission) throws Exception {
        return grant(null, packageName, permission);
    }

    /**
     * Revoke a permission from an app.
     * <p>
     * On devices running Android 6.0 (API level 23) and higher, the permission can be any permission declared in the app manifest.
     * On devices running Android 5.1 (API level 22) and lower, must be an optional permission defined by the app.
     *
     * @param target
     * @param packageName
     * @param permission
     * @return
     * @throws Exception
     */
    public String revoke(Target target, @NonNull String packageName, @NonNull String permission) throws Exception {
        CommandLine command = create(target)
                .addArgument(REVOKE)
                .addArgument(packageName)
                .addArgument(permission);
        return CommandLineTools.exec(command);
    }

    public String revoke(@NonNull String packageName, @NonNull String permission) throws Exception {
        return revoke(null, packageName, permission);
    }

    /**
     * Deletes all data associated with a package.
     *
     * @param target
     * @param packageName
     * @return
     * @throws Exception
     */
    public String clear(Target target, @NonNull String packageName) throws Exception {
        CommandLine command = create(target)
                .addArgument(CLEAR)
                .addArgument(packageName);
        return CommandLineTools.exec(command);
    }

    public String clear(@NonNull String packageName) throws Exception {
        return clear(packageName);
    }

}
