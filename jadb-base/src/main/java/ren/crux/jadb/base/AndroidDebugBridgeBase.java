/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.base;

import lombok.NonNull;
import org.apache.commons.exec.CommandLine;
import ren.crux.jadb.Consts;
import ren.crux.jadb.model.Target;
import ren.crux.jadb.util.ArgumentHelper;
import ren.crux.jadb.util.CommandLineTools;

import java.io.File;

import static ren.crux.jadb.Consts.Command.*;

/**
 * @author wangzhihui
 */
public class AndroidDebugBridgeBase {

    private String adbShellPath;

    public AndroidDebugBridgeBase() {
        adbShellPath = Consts.ADB;
    }

    public AndroidDebugBridgeBase(String adbShellPath) {
        this.adbShellPath = adbShellPath;
    }

    protected CommandLine create() {
        return create(null);
    }

    protected CommandLine create(Target target) {
        return ArgumentHelper.addTarget(new CommandLine(adbShellPath), target);
    }

    /**
     * Print a list of all devices.
     * Use the -l option to include the device descriptions.
     *
     * @return
     * @throws Exception
     */
    public String devices() throws Exception {
        CommandLine command = create()
                .addArgument(DEVICES)
                .addArgument(Device.LIST);
        return CommandLineTools.exec(command);
    }

    /**
     * Connect to a device over TCP/IP.
     * <p>
     * If you do not specify a port, then the default port, 5555, is used.
     *
     * @param serialNumber
     * @return
     * @throws Exception
     */
    public String connect(@NonNull String serialNumber) throws Exception {
        CommandLine command = create()
                .addArgument(CONNECT)
                .addArgument(serialNumber);
        return CommandLineTools.exec(command);
    }

    /**
     * Disconnect from the specified TCP/IP device running on the specified port.
     * <p>
     * If you do not specify a host or a port, then all devices are disconnected from all TCP/IP ports.
     * If you specify a host, but not a port, the default port, 5555, is used.
     *
     * @param serialNumber
     * @return
     * @throws Exception
     */
    public String disconnect(@NonNull String serialNumber) throws Exception {
        CommandLine command = create()
                .addArgument(DISCONNECT)
                .addArgument(serialNumber);
        return CommandLineTools.exec(command);
    }

    /**
     * Installs a package (specified by path) to the system.
     *
     * @param target
     * @param path
     * @param grantAllPermissions Grant all runtime permissions.
     * @param replace             Replace the existing app.
     * @return
     * @throws Exception
     */
    public String install(Target target, @NonNull String path, boolean grantAllPermissions, boolean replace) throws Exception {
        if (!new File(path).exists()) {
            throw new IllegalArgumentException("Invalid path : " + path);
        }
        CommandLine command = create(target)
                .addArgument(INSTALL);
        if (grantAllPermissions) {
            command.addArgument(Consts.Option.App.Install.GRANT_ALL);
        }
        if (replace) {
            command.addArgument(Consts.Option.App.Install.REPLACE);
        }
        command.addArgument(path);
        return CommandLineTools.exec(command);
    }

    public String install(@NonNull String path, boolean grantAllPermissions, boolean replace) throws Exception {
        return install(null, path, grantAllPermissions, replace);
    }

    /**
     * Remove this app package from the device.
     *
     * @param target
     * @param packageName
     * @param keep        keep the data and cache directories.
     * @return
     * @throws Exception
     */
    public String uninstall(Target target, @NonNull String packageName, boolean keep) throws Exception {
        CommandLine command = create(target)
                .addArgument(UNINSTALL);
        if (keep) {
            command.addArgument(Consts.Option.App.Uninstall.KEEP);
        }
        command.addArgument(packageName);
        return CommandLineTools.exec(command);
    }

    public String uninstall(@NonNull String packageName, boolean keep) throws Exception {
        return uninstall(null, packageName, keep);
    }

    /**
     * Restart the adb server listening on TCP at the specified port.
     *
     * @param target
     * @param port
     * @return
     * @throws Exception
     */
    public String tcpip(Target target, int port) throws Exception {
        if (port <= 0) {
            throw new IllegalArgumentException("Invalid port : " + port);
        }
        CommandLine command = create(target)
                .addArgument(TCPIP)
                .addArgument(String.valueOf(port));
        return CommandLineTools.exec(command);
    }

    public String tcpip(int port) throws Exception {
        return tcpip(null, port);
    }

    /**
     * Restart the adb server listening on USB.
     *
     * @param target
     * @return
     * @throws Exception
     */
    public String usb(Target target) throws Exception {
        CommandLine command = create(target)
                .addArgument(USB);
        return CommandLineTools.exec(command);
    }

    public String usb() throws Exception {
        return usb(null);
    }
}
