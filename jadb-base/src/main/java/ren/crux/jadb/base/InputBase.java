/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.base;

import lombok.NonNull;
import org.apache.commons.exec.CommandLine;
import ren.crux.jadb.Consts;
import ren.crux.jadb.model.KeyEvent;
import ren.crux.jadb.model.Target;
import ren.crux.jadb.util.CommandLineTools;

/**
 * @author wangzhihui
 */
public class InputBase {

    protected final ShellBase base;

    public InputBase(ShellBase base) {
        this.base = base;
    }

    protected CommandLine create(Target target) {
        return base.create(target)
                .addArgument(Consts.Command.Shell.INPUT);
    }

    /**
     * Inputs key event.
     *
     * @param target
     * @param keyEvent
     * @return
     * @throws Exception
     */
    public String keyevent(Target target, @NonNull KeyEvent keyEvent) throws Exception {
        CommandLine command = create(target)
                .addArgument(Consts.Command.Shell.Input.KEY_EVENT)
                .addArgument(keyEvent.toString());
        return CommandLineTools.exec(command);
    }

    public String keyevent(@NonNull KeyEvent keyEvent) throws Exception {
        return keyevent(null, keyEvent);
    }

    /**
     * Inputs text.
     *
     * @param target
     * @param content
     * @return
     * @throws Exception
     */
    public String text(Target target, @NonNull String content) throws Exception {
        CommandLine command = create(target)
                .addArgument(Consts.Command.Shell.Input.TEXT)
                .addArgument(content);
        return CommandLineTools.exec(command);
    }

    public String text(@NonNull String content) throws Exception {
        return text(null, content);
    }

    /**
     * Inputs a tap event at the specific coordinates in pixels.
     *
     * @param target
     * @param x
     * @param y
     * @return
     * @throws Exception
     */
    public String tap(Target target, int x, int y) throws Exception {
        CommandLine command = create(target)
                .addArgument(Consts.Command.Shell.Input.TAP)
                .addArgument(String.valueOf(x))
                .addArgument(String.valueOf(y));
        return CommandLineTools.exec(command);
    }

    public String tap(int x, int y) throws Exception {
        return tap(null, x, y);
    }

    /**
     * Inputs a swipe gesture starting at a set of coordinates, ending at another set of coordinates.
     *
     * @param target
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     * @throws Exception
     */
    public String swipe(Target target, int x1, int y1, int x2, int y2) throws Exception {
        CommandLine command = create(target)
                .addArgument(Consts.Command.Shell.Input.SWIPE)
                .addArgument(String.valueOf(x1))
                .addArgument(String.valueOf(y1))
                .addArgument(String.valueOf(x2))
                .addArgument(String.valueOf(y2));
        return CommandLineTools.exec(command);
    }

    public String swipe(int x1, int y1, int x2, int y2) throws Exception {
        return swipe(null, x1, y1, x2, y2);
    }

}
