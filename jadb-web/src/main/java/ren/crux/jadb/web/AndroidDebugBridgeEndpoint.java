/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.web;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ren.crux.jadb.AndroidDebugBridge;
import ren.crux.jadb.model.Device;
import ren.crux.jadb.model.Intent;
import ren.crux.jadb.model.KeyEvent;
import ren.crux.jadb.model.Target;
import ren.crux.web.common.RestWebResultContainer;
import ren.crux.web.common.RestWebResults;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * @author wangzhihui
 */
@RestController
@RequestMapping("/adb")
public class AndroidDebugBridgeEndpoint {

    @Autowired
    private AndroidDebugBridge adb;

    @GetMapping("/devices")
    public List<Device> devices() throws Exception {
        return adb.devices();
    }

    @PostMapping("/tcpip/{port}")
    public RestWebResultContainer tcpip(Target target, @PathVariable(required = false) Integer port) throws Exception {
        if (port == null) {
            port = 5555;
        }
        return RestWebResults.operation(adb.tcpip(target, port));
    }

    @PostMapping("/usb")
    public RestWebResultContainer usb(Target target) throws Exception {
        return RestWebResults.operation(adb.usb(target));
    }

    @PostMapping("/connect/{host}/{port}")
    public RestWebResultContainer connect(@PathVariable String host, @PathVariable int port) throws Exception {
        return RestWebResults.operation(adb.connect(host, port));
    }

    @PostMapping("/disconnect/{host}/{port}")
    public RestWebResultContainer disconnect(@PathVariable String host, @PathVariable int port) throws Exception {
        return RestWebResults.operation(adb.disconnect(host, port));
    }

    @PostMapping("/install")
    public RestWebResultContainer install(Target target, @RequestParam(defaultValue = "false") boolean grantAll,
                                          @RequestParam(defaultValue = "true") boolean replace, @RequestParam("file") MultipartFile file) throws Exception {
        String filename = file.getOriginalFilename();
        File tmpFile = File.createTempFile(StringUtils.defaultString(filename, "app"), "apk");
        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tmpFile))) {
            out.write(file.getBytes());
            out.flush();
        }
        return RestWebResults.operation(adb.install(target, tmpFile.getPath(), grantAll, replace));
    }

    @PostMapping("/uninstall/{packageName}")
    public RestWebResultContainer uninstall(Target target, @PathVariable String packageName, @RequestParam(defaultValue = "false") boolean keep) throws Exception {
        return RestWebResults.operation(adb.uninstall(target, packageName, keep));
    }

    /*
     *
     * adb shell pm
     *
     * */

    @GetMapping("/shell/pm/list/packages")
    public List<String> listPackages(Target target, Boolean dangerous, String filter) throws Exception {
        return adb.shell.pm.listPackages(target, dangerous, filter);
    }

    @GetMapping("/shell/pm/list/permission-groups")
    public List<String> listPermissionGroups(Target target) throws Exception {
        return adb.shell.pm.listPermissionGroups(target);
    }

    @GetMapping("/shell/pm/list/permissions")
    public List<String> listPermissions(Target target, @RequestParam(defaultValue = "false") boolean dangerous) throws Exception {
        return adb.shell.pm.listPermissions(target, dangerous);
    }

    @PostMapping("/shell/pm/clear/{packageName}")
    public RestWebResultContainer clear(Target target, @PathVariable String packageName) throws Exception {
        return RestWebResults.operation(adb.shell.pm.clear(target, packageName));
    }

    @PostMapping("/shell/pm/grant/{packageName}/{permission}")
    public RestWebResultContainer grant(Target target, @PathVariable String packageName, @PathVariable String permission) throws Exception {
        return RestWebResults.operation(adb.shell.pm.grant(target, packageName, permission));
    }

    @PostMapping("/shell/pm/revoke/{packageName}/{permission}")
    public RestWebResultContainer revoke(Target target, @PathVariable String packageName, @PathVariable String permission) throws Exception {
        return RestWebResults.operation(adb.shell.pm.revoke(target, packageName, permission));
    }

    /*
     *
     * adb shell am
     *
     * */

    @PostMapping("/shell/am/force-stop/{packageName}")
    public RestWebResultContainer forceStop(Target target, @PathVariable String packageName) throws Exception {
        return RestWebResults.operation(adb.shell.am.forceStop(target, packageName));
    }

    @PostMapping("/shell/am/kill/{packageName}")
    public RestWebResultContainer kill(Target target, @PathVariable String packageName) throws Exception {
        return RestWebResults.operation(adb.shell.am.kill(target, packageName));
    }

    @PostMapping("/shell/am/kill-all")
    public RestWebResultContainer killAll(Target target) throws Exception {
        return RestWebResults.operation(adb.shell.am.killAll(target));
    }

    @PostMapping("/shell/am/start")
    public RestWebResultContainer start(Target target, @RequestParam(defaultValue = "false") boolean debug,
                                        @RequestParam(defaultValue = "false") boolean wait, @RequestParam(defaultValue = "0") int repeat,
                                        @RequestParam(defaultValue = "false") boolean stop, @RequestParam Intent intent) throws Exception {
        return RestWebResults.operation(adb.shell.am.start(target, debug, wait, repeat, stop, intent));
    }

    @PostMapping("/shell/am/startservice")
    public RestWebResultContainer startService(Target target, @RequestParam Intent intent) throws Exception {
        return RestWebResults.operation(adb.shell.am.startService(target, intent));
    }

    @PostMapping("/shell/am/broadcast")
    public RestWebResultContainer broadcast(Target target, @RequestParam Intent intent) throws Exception {
        return RestWebResults.operation(adb.shell.am.broadcast(target, intent));
    }

    @GetMapping("/shell/am/to-uri")
    public String toUri(@RequestParam Intent intent) throws Exception {
        return adb.shell.am.toUri(intent);
    }

    @GetMapping("/shell/am/to-intent-uri")
    public String toIntentUri(@RequestParam Intent intent) throws Exception {
        return adb.shell.am.toIntentUri(intent);
    }

    /*
     *
     * adb shell input
     *
     * */

    @PostMapping("/shell/input/keyevent/{keyEvent}")
    public RestWebResultContainer keyEvent(Target target, @PathVariable KeyEvent keyEvent) throws Exception {
        return RestWebResults.operation(adb.shell.input.keyevent(target, keyEvent));
    }

    @PostMapping("/shell/input/text")
    public RestWebResultContainer text(Target target, @RequestParam String content) throws Exception {
        return RestWebResults.operation(adb.shell.input.text(target, content));
    }

    @PostMapping("/shell/input/tap/{x}/{y}")
    public RestWebResultContainer tap(Target target, @PathVariable int x, @PathVariable int y) throws Exception {
        return RestWebResults.operation(adb.shell.input.tap(target, x, y));
    }

    @PostMapping("/shell/input/swipe/{x1}/{y1}/{x2}/{y2}")
    public RestWebResultContainer swipe(Target target, @PathVariable int x1, @PathVariable int y1,
                                        @PathVariable int x2, @PathVariable int y2) throws Exception {
        return RestWebResults.operation(adb.shell.input.swipe(target, x1, y1, x2, y2));
    }

}
