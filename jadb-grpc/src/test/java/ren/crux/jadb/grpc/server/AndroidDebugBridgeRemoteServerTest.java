/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.server;

import org.junit.Test;
import ren.crux.jadb.grpc.Jadb;
import ren.crux.jadb.grpc.client.AndroidDebugBridgeClient;

import java.io.IOException;

public class AndroidDebugBridgeRemoteServerTest {

    private int port = 1111;
    private AndroidDebugBridgeRemoteServer server = new AndroidDebugBridgeRemoteServer(port);
    private AndroidDebugBridgeClient client = new AndroidDebugBridgeClient("localhost", port);

    @Test
    public void start() throws IOException, InterruptedException {
        server.start();
        Jadb.Devices devices = client.adb.devices(Jadb.Void.newBuilder().build());
        devices.getDevicesList().forEach(System.out::println);
        server.blockUntilShutdown();
    }

}