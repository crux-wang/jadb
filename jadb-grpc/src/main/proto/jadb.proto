syntax = "proto3";
package ren.crux.jadb.grpc;

enum TargetType {
    /**
    * Direct an adb command to the only attached USB device.
    * Returns an error when more than one USB device is attached.
    */
    DEVICE = 0;
    /**
     * Direct an adb command to the only running emulator.
     * Returns an error when more than one emulator is running.
     */
    EMULATOR = 1;
    /**
     * Direct an adb command to a specific device, referred to by its adb-assigned serial number =such as emulator-5556).
     * Overrides the serial number value stored in the $ANDROID_SERIAL environment variable.
     */
    SERIAL_NUMBER = 2;
}

message Target {
    TargetType type = 1;
    string serialNumber = 2;
    string packageName = 3;
    Intent intent = 4;

}

enum DeviceState {
    offline = 0;
    device = 1;
    no_device = 2;
    unauthorized = 3;
}

message Device {

    string serialNumber = 1;
    DeviceState state = 2;
    string usb = 3;
    string product = 4;
    string model = 5;
    string deviceName = 6;
    int32 transportId = 7;
    bool emulator = 8;

}

enum Flags {
    FLAG_GRANT_READ_URI_PERMISSION = 0;
    FLAG_GRANT_WRITE_URI_PERMISSION = 1;
    FLAG_DEBUG_LOG_RESOLUTION = 2;
    FLAG_EXCLUDE_STOPPED_PACKAGES = 3;
    FLAG_INCLUDE_STOPPED_PACKAGES = 4;
    FLAG_ACTIVITY_BROUGHT_TO_FRONT = 5;
    FLAG_ACTIVITY_CLEAR_TOP = 6;
    FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET = 7;
    FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS = 8;
    FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY = 9;
    FLAG_ACTIVITY_MULTIPLE_TASK = 10;
    FLAG_ACTIVITY_NO_ANIMATION = 11;
    FLAG_ACTIVITY_NO_HISTORY = 12;
    FLAG_ACTIVITY_NO_USER_ACTION = 13;
    FLAG_ACTIVITY_PREVIOUS_IS_TOP = 14;
    FLAG_ACTIVITY_REORDER_TO_FRONT = 15;
    FLAG_ACTIVITY_RESET_TASK_IF_NEEDED = 16;
    FLAG_ACTIVITY_SINGLE_TOP = 17;
    FLAG_ACTIVITY_CLEAR_TASK = 18;
    FLAG_ACTIVITY_TASK_ON_HOME = 19;
    FLAG_RECEIVER_REGISTERED_ONLY = 20;
    FLAG_RECEIVER_REPLACE_PENDING = 21;
}

enum KeyEvent {

    ACTION_DOWN = 0;
    ACTION_MULTIPLE = 1;
    ACTION_UP = 2;
    FLAG_CANCELED = 3;
    FLAG_CANCELED_LONG_PRESS = 4;
    FLAG_EDITOR_ACTION = 5;
    FLAG_FALLBACK = 6;
    FLAG_FROM_SYSTEM = 7;
    FLAG_KEEP_TOUCH_MODE = 8;
    FLAG_LONG_PRESS = 9;
    FLAG_SOFT_KEYBOARD = 10;
    FLAG_TRACKING = 11;
    FLAG_VIRTUAL_HARD_KEY = 12;
    //    @Deprecated
    FLAG_WOKE_HERE = 13;
    KEYCODE_0 = 14;
    KEYCODE_1 = 15;
    KEYCODE_11 = 16;
    KEYCODE_12 = 17;
    KEYCODE_2 = 18;
    KEYCODE_3 = 19;
    KEYCODE_3D_MODE = 20;
    KEYCODE_4 = 21;
    KEYCODE_5 = 22;
    KEYCODE_6 = 23;
    KEYCODE_7 = 24;
    KEYCODE_8 = 25;
    KEYCODE_9 = 26;
    KEYCODE_A = 27;
    KEYCODE_ALT_LEFT = 28;
    KEYCODE_ALT_RIGHT = 29;
    KEYCODE_APOSTROPHE = 30;
    KEYCODE_APP_SWITCH = 31;
    KEYCODE_ASSIST = 32;
    KEYCODE_AT = 33;
    KEYCODE_AVR_INPUT = 34;
    KEYCODE_AVR_POWER = 35;
    KEYCODE_B = 36;
    KEYCODE_BACK = 37;
    KEYCODE_BACKSLASH = 38;
    KEYCODE_BOOKMARK = 39;
    KEYCODE_BREAK = 40;
    KEYCODE_BRIGHTNESS_DOWN = 41;
    KEYCODE_BRIGHTNESS_UP = 42;
    KEYCODE_BUTTON_1 = 43;
    KEYCODE_BUTTON_10 = 44;
    KEYCODE_BUTTON_11 = 45;
    KEYCODE_BUTTON_12 = 46;
    KEYCODE_BUTTON_13 = 47;
    KEYCODE_BUTTON_14 = 48;
    KEYCODE_BUTTON_15 = 49;
    KEYCODE_BUTTON_16 = 50;
    KEYCODE_BUTTON_2 = 51;
    KEYCODE_BUTTON_3 = 52;
    KEYCODE_BUTTON_4 = 53;
    KEYCODE_BUTTON_5 = 54;
    KEYCODE_BUTTON_6 = 55;
    KEYCODE_BUTTON_7 = 56;
    KEYCODE_BUTTON_8 = 57;
    KEYCODE_BUTTON_9 = 58;
    KEYCODE_BUTTON_A = 59;
    KEYCODE_BUTTON_B = 60;
    KEYCODE_BUTTON_C = 61;
    KEYCODE_BUTTON_L1 = 62;
    KEYCODE_BUTTON_L2 = 63;
    KEYCODE_BUTTON_MODE = 64;
    KEYCODE_BUTTON_R1 = 65;
    KEYCODE_BUTTON_R2 = 66;
    KEYCODE_BUTTON_SELECT = 67;
    KEYCODE_BUTTON_START = 68;
    KEYCODE_BUTTON_THUMBL = 69;
    KEYCODE_BUTTON_THUMBR = 70;
    KEYCODE_BUTTON_X = 71;
    KEYCODE_BUTTON_Y = 72;
    KEYCODE_BUTTON_Z = 73;
    KEYCODE_C = 74;
    KEYCODE_CALCULATOR = 75;
    KEYCODE_CALENDAR = 76;
    KEYCODE_CALL = 77;
    KEYCODE_CAMERA = 78;
    KEYCODE_CAPS_LOCK = 79;
    KEYCODE_CAPTIONS = 80;
    KEYCODE_CHANNEL_DOWN = 81;
    KEYCODE_CHANNEL_UP = 82;
    KEYCODE_CLEAR = 83;
    KEYCODE_COMMA = 84;
    KEYCODE_CONTACTS = 85;
    KEYCODE_COPY = 86;
    KEYCODE_CTRL_LEFT = 87;
    KEYCODE_CTRL_RIGHT = 88;
    KEYCODE_CUT = 89;
    KEYCODE_D = 90;
    KEYCODE_DEL = 91;
    KEYCODE_DPAD_CENTER = 92;
    KEYCODE_DPAD_DOWN = 93;
    KEYCODE_DPAD_DOWN_LEFT = 94;
    KEYCODE_DPAD_DOWN_RIGHT = 95;
    KEYCODE_DPAD_LEFT = 96;
    KEYCODE_DPAD_RIGHT = 97;
    KEYCODE_DPAD_UP = 98;
    KEYCODE_DPAD_UP_LEFT = 99;
    KEYCODE_DPAD_UP_RIGHT = 100;
    KEYCODE_DVR = 101;
    KEYCODE_E = 102;
    KEYCODE_EISU = 103;
    KEYCODE_ENDCALL = 104;
    KEYCODE_ENTER = 105;
    KEYCODE_ENVELOPE = 106;
    KEYCODE_EQUALS = 107;
    KEYCODE_ESCAPE = 108;
    KEYCODE_EXPLORER = 109;
    KEYCODE_F = 110;
    KEYCODE_F1 = 111;
    KEYCODE_F10 = 112;
    KEYCODE_F11 = 113;
    KEYCODE_F12 = 114;
    KEYCODE_F2 = 115;
    KEYCODE_F3 = 116;
    KEYCODE_F4 = 117;
    KEYCODE_F5 = 118;
    KEYCODE_F6 = 119;
    KEYCODE_F7 = 120;
    KEYCODE_F8 = 121;
    KEYCODE_F9 = 122;
    KEYCODE_FOCUS = 123;
    KEYCODE_FORWARD = 124;
    KEYCODE_FORWARD_DEL = 125;
    KEYCODE_FUNCTION = 126;
    KEYCODE_G = 127;
    KEYCODE_GRAVE = 128;
    KEYCODE_GUIDE = 129;
    KEYCODE_H = 130;
    KEYCODE_HEADSETHOOK = 131;
    KEYCODE_HELP = 132;
    KEYCODE_HENKAN = 133;
    KEYCODE_HOME = 134;
    KEYCODE_I = 135;
    KEYCODE_INFO = 136;
    KEYCODE_INSERT = 137;
    KEYCODE_J = 138;
    KEYCODE_K = 139;
    KEYCODE_KANA = 140;
    KEYCODE_KATAKANA_HIRAGANA = 141;
    KEYCODE_L = 142;
    KEYCODE_LANGUAGE_SWITCH = 143;
    KEYCODE_LAST_CHANNEL = 144;
    KEYCODE_LEFT_BRACKET = 145;
    KEYCODE_M = 146;
    KEYCODE_MANNER_MODE = 147;
    KEYCODE_MEDIA_AUDIO_TRACK = 148;
    KEYCODE_MEDIA_CLOSE = 149;
    KEYCODE_MEDIA_EJECT = 150;
    KEYCODE_MEDIA_FAST_FORWARD = 151;
    KEYCODE_MEDIA_NEXT = 152;
    KEYCODE_MEDIA_PAUSE = 153;
    KEYCODE_MEDIA_PLAY = 154;
    KEYCODE_MEDIA_PLAY_PAUSE = 155;
    KEYCODE_MEDIA_PREVIOUS = 156;
    KEYCODE_MEDIA_RECORD = 157;
    KEYCODE_MEDIA_REWIND = 158;
    KEYCODE_MEDIA_SKIP_BACKWARD = 159;
    KEYCODE_MEDIA_SKIP_FORWARD = 160;
    KEYCODE_MEDIA_STEP_BACKWARD = 161;
    KEYCODE_MEDIA_STEP_FORWARD = 162;
    KEYCODE_MEDIA_STOP = 163;
    KEYCODE_MEDIA_TOP_MENU = 164;
    KEYCODE_MENU = 165;
    KEYCODE_META_LEFT = 166;
    KEYCODE_META_RIGHT = 167;
    KEYCODE_MINUS = 168;
    KEYCODE_MOVE_END = 169;
    KEYCODE_MOVE_HOME = 170;
    KEYCODE_MUHENKAN = 171;
    KEYCODE_MUSIC = 172;
    KEYCODE_MUTE = 173;
    KEYCODE_N = 174;
    KEYCODE_NAVIGATE_IN = 175;
    KEYCODE_NAVIGATE_NEXT = 176;
    KEYCODE_NAVIGATE_OUT = 177;
    KEYCODE_NAVIGATE_PREVIOUS = 178;
    KEYCODE_NOTIFICATION = 179;
    KEYCODE_NUM = 180;
    KEYCODE_NUMPAD_0 = 181;
    KEYCODE_NUMPAD_1 = 182;
    KEYCODE_NUMPAD_2 = 183;
    KEYCODE_NUMPAD_3 = 184;
    KEYCODE_NUMPAD_4 = 185;
    KEYCODE_NUMPAD_5 = 186;
    KEYCODE_NUMPAD_6 = 187;
    KEYCODE_NUMPAD_7 = 188;
    KEYCODE_NUMPAD_8 = 189;
    KEYCODE_NUMPAD_9 = 190;
    KEYCODE_NUMPAD_ADD = 191;
    KEYCODE_NUMPAD_COMMA = 192;
    KEYCODE_NUMPAD_DIVIDE = 193;
    KEYCODE_NUMPAD_DOT = 194;
    KEYCODE_NUMPAD_ENTER = 195;
    KEYCODE_NUMPAD_EQUALS = 196;
    KEYCODE_NUMPAD_LEFT_PAREN = 197;
    KEYCODE_NUMPAD_MULTIPLY = 198;
    KEYCODE_NUMPAD_RIGHT_PAREN = 199;
    KEYCODE_NUMPAD_SUBTRACT = 200;
    KEYCODE_NUM_LOCK = 201;
    KEYCODE_O = 202;
    KEYCODE_P = 203;
    KEYCODE_PAGE_DOWN = 204;
    KEYCODE_PAGE_UP = 205;
    KEYCODE_PAIRING = 206;
    KEYCODE_PASTE = 207;
    KEYCODE_PERIOD = 208;
    KEYCODE_PICTSYMBOLS = 209;
    KEYCODE_PLUS = 210;
    KEYCODE_POUND = 211;
    KEYCODE_POWER = 212;
    KEYCODE_PROG_BLUE = 213;
    KEYCODE_PROG_GREEN = 214;
    KEYCODE_PROG_RED = 215;
    KEYCODE_PROG_YELLOW = 216;
    KEYCODE_Q = 217;
    KEYCODE_R = 218;
    KEYCODE_RIGHT_BRACKET = 219;
    KEYCODE_RO = 220;
    KEYCODE_S = 221;
    KEYCODE_SCROLL_LOCK = 222;
    KEYCODE_SEARCH = 223;
    KEYCODE_SEMICOLON = 224;
    KEYCODE_SETTINGS = 225;
    KEYCODE_SHIFT_LEFT = 226;
    KEYCODE_SHIFT_RIGHT = 227;
    KEYCODE_SLASH = 228;
    KEYCODE_SLEEP = 229;
    KEYCODE_SOFT_LEFT = 230;
    KEYCODE_SOFT_RIGHT = 231;
    KEYCODE_SOFT_SLEEP = 232;
    KEYCODE_SPACE = 233;
    KEYCODE_STAR = 234;
    KEYCODE_STB_INPUT = 235;
    KEYCODE_STB_POWER = 236;
    KEYCODE_STEM_1 = 237;
    KEYCODE_STEM_2 = 238;
    KEYCODE_STEM_3 = 239;
    KEYCODE_STEM_PRIMARY = 240;
    KEYCODE_SWITCH_CHARSET = 241;
    KEYCODE_SYM = 242;
    KEYCODE_SYSRQ = 243;
    KEYCODE_SYSTEM_NAVIGATION_DOWN = 244;
    KEYCODE_SYSTEM_NAVIGATION_LEFT = 245;
    KEYCODE_SYSTEM_NAVIGATION_RIGHT = 246;
    KEYCODE_SYSTEM_NAVIGATION_UP = 247;
    KEYCODE_T = 248;
    KEYCODE_TAB = 249;
    KEYCODE_TV = 250;
    KEYCODE_TV_ANTENNA_CABLE = 251;
    KEYCODE_TV_AUDIO_DESCRIPTION = 252;
    KEYCODE_TV_AUDIO_DESCRIPTION_MIX_DOWN = 253;
    KEYCODE_TV_AUDIO_DESCRIPTION_MIX_UP = 254;
    KEYCODE_TV_CONTENTS_MENU = 255;
    KEYCODE_TV_DATA_SERVICE = 256;
    KEYCODE_TV_INPUT = 257;
    KEYCODE_TV_INPUT_COMPONENT_1 = 258;
    KEYCODE_TV_INPUT_COMPONENT_2 = 259;
    KEYCODE_TV_INPUT_COMPOSITE_1 = 260;
    KEYCODE_TV_INPUT_COMPOSITE_2 = 261;
    KEYCODE_TV_INPUT_HDMI_1 = 262;
    KEYCODE_TV_INPUT_HDMI_2 = 263;
    KEYCODE_TV_INPUT_HDMI_3 = 264;
    KEYCODE_TV_INPUT_HDMI_4 = 265;
    KEYCODE_TV_INPUT_VGA_1 = 266;
    KEYCODE_TV_MEDIA_CONTEXT_MENU = 267;
    KEYCODE_TV_NETWORK = 268;
    KEYCODE_TV_NUMBER_ENTRY = 269;
    KEYCODE_TV_POWER = 270;
    KEYCODE_TV_RADIO_SERVICE = 271;
    KEYCODE_TV_SATELLITE = 272;
    KEYCODE_TV_SATELLITE_BS = 273;
    KEYCODE_TV_SATELLITE_CS = 274;
    KEYCODE_TV_SATELLITE_SERVICE = 275;
    KEYCODE_TV_TELETEXT = 276;
    KEYCODE_TV_TERRESTRIAL_ANALOG = 277;
    KEYCODE_TV_TERRESTRIAL_DIGITAL = 278;
    KEYCODE_TV_TIMER_PROGRAMMING = 279;
    KEYCODE_TV_ZOOM_MODE = 280;
    KEYCODE_U = 281;
    KEYCODE_UNKNOWN = 282;
    KEYCODE_V = 283;
    KEYCODE_VOICE_ASSIST = 284;
    KEYCODE_VOLUME_DOWN = 285;
    KEYCODE_VOLUME_MUTE = 286;
    KEYCODE_VOLUME_UP = 287;
    KEYCODE_W = 288;
    KEYCODE_WAKEUP = 289;
    KEYCODE_WINDOW = 290;
    KEYCODE_X = 291;
    KEYCODE_Y = 292;
    KEYCODE_YEN = 293;
    KEYCODE_Z = 294;
    KEYCODE_ZENKAKU_HANKAKU = 295;
    KEYCODE_ZOOM_IN = 296;
    KEYCODE_ZOOM_OUT = 297;
    //    @Deprecated
    MAX_KEYCODE = 298;
    META_ALT_LEFT_ON = 299;
    META_ALT_MASK = 300;
    META_ALT_ON = 301;
    META_ALT_RIGHT_ON = 302;
    META_CAPS_LOCK_ON = 303;
    META_CTRL_LEFT_ON = 304;
    META_CTRL_MASK = 305;
    META_CTRL_ON = 306;
    META_CTRL_RIGHT_ON = 307;
    META_FUNCTION_ON = 308;
    META_META_LEFT_ON = 309;
    META_META_MASK = 310;
    META_META_ON = 311;
    META_META_RIGHT_ON = 312;
    META_NUM_LOCK_ON = 313;
    META_SCROLL_LOCK_ON = 314;
    META_SHIFT_LEFT_ON = 315;
    META_SHIFT_MASK = 316;
    META_SHIFT_ON = 317;
    META_SHIFT_RIGHT_ON = 318;
    META_SYM_ON = 319;

}

enum IntentExtraValueTypes {
    _string = 0;
    _int = 1;
    _long = 2;
    _float = 3;
    _bool = 4;
    _int_array = 5;
    _long_array = 6;
    _float_array = 7;
}

message IntentExtraValue {
    IntentExtraValueTypes type = 1;
    string stringVal = 2;
    int32 intVal = 3;
    int64 longVal = 4;
    float floatVal = 5;
    bool boolVal = 6;
    repeated int32 intArrayVal = 7;
    repeated int64 longArrayVal = 8;
    repeated float floatArrayVal = 9;
}


message Intent {

    string action = 1;
    string dataUri = 2;
    string category = 3;
    string component = 4;
    int32 flagsValue = 5;
    string mimeType = 6;
    map<string, IntentExtraValue> extras = 7;
    bool selector = 8;
    repeated Flags flags = 9;

}

message Void {

}


message FileTuple {
    string name = 1;
    bytes bytes = 2;
}

message InstallRequest {
    Target target = 1;
    FileTuple apk = 2;
    bool grantAllPermissions = 3;
    bool replace = 4;
}

message UninstallRequest {
    Target target = 1;
    bool keep = 2;
}

message Devices {
    repeated Device devices = 1;
}

message StartActivityRequest {
    Target target = 1;
    bool debug = 2;
    bool wait = 3;
    int32 repeat = 4;
    bool stop = 5;
}

message Strings {
    repeated string list = 1;
}

enum PackageFilter {
    none = 0;
    system = 1;
    third_party = 2;
}

message ListPackagesRequest {
    Target target = 1;
    PackageFilter option = 2;
    string filter = 3;
}

message listPermissionsRequest {
    Target target = 1;
    bool dangerous = 2;
}

message PermissionsRequest {
    Target target = 1;
    string permissions = 2;
}

message KeyEventRequest {
    Target target = 1;
    KeyEvent key = 2;
}

message InputTextRequest {
    Target target = 1;
    string content = 2;
}

message Point {
    int32 x = 1;
    int32 y = 2;
}

message PointTuple {
    Point form = 1;
    Point to = 2;
}

message TapRequest {
    Target target = 1;
    Point point = 2;
}

message SwipeRequest {
    Target target = 1;
    PointTuple tuple = 2;
}

message Port {
    int32 port = 1;
}

message Bool {
    bool success = 1;
}

message Str {
    string string = 1;
}

service AndroidDebugBridgeService {

    rpc devices (Void) returns (Devices) {
    }
    rpc tcpip (Port) returns (Bool) {
    }
    rpc connect (Target) returns (Bool) {
    }
    rpc disconnect (Target) returns (Bool) {
    }
    rpc usb (Target) returns (Bool) {
    }
    rpc install (InstallRequest) returns (Bool) {
    }
    rpc uninstall (UninstallRequest) returns (Bool) {
    }

}

service ActivityManagerService {

    rpc forceStop (Target) returns (Bool) {
    }
    rpc kill (Target) returns (Bool) {
    }
    rpc killAll (Target) returns (Bool) {
    }
    rpc start (StartActivityRequest) returns (Bool) {
    }
    rpc startService (Target) returns (Bool) {
    }
    rpc broadcast (Target) returns (Bool) {
    }
    rpc toUri (Intent) returns (Str) {
    }
    rpc toIntentUri (Intent) returns (Str) {
    }
}

service PackageManagerService {

    rpc listPackages (ListPackagesRequest) returns (Strings) {
    }
    rpc listPermissionGroups (Target) returns (Strings) {
    }
    rpc listPermissions (listPermissionsRequest) returns (Strings) {
    }
    rpc grant (PermissionsRequest) returns (Bool) {
    }
    rpc revoke (PermissionsRequest) returns (Bool) {
    }
    rpc clear (Target) returns (Bool) {
    }
}

service InputService {

    rpc keyevent (KeyEventRequest) returns (Bool) {
    }
    rpc text (InputTextRequest) returns (Bool) {
    }
    rpc tap (TapRequest) returns (Bool) {
    }
    rpc swipe (SwipeRequest) returns (Bool) {
    }

}