/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import ren.crux.jadb.grpc.impl.ActivityManagerServiceImpl;
import ren.crux.jadb.grpc.impl.AndroidDebugBridgeServiceImpl;
import ren.crux.jadb.grpc.impl.InputServiceImpl;
import ren.crux.jadb.grpc.impl.PackageManagerServiceImpl;

import java.io.IOException;

/**
 * @author wangzhihui
 */
@Slf4j
public class AndroidDebugBridgeRemoteServer {
    /* The port on which the server should run */
    private int port = 50051;
    private Server server;

    public AndroidDebugBridgeRemoteServer() {
    }

    public AndroidDebugBridgeRemoteServer(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        server = ServerBuilder.forPort(port)
                .addService(new AndroidDebugBridgeServiceImpl())
                .addService(new ActivityManagerServiceImpl())
                .addService(new PackageManagerServiceImpl())
                .addService(new InputServiceImpl())
                .build()
                .start();
        log.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                AndroidDebugBridgeRemoteServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    public void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
