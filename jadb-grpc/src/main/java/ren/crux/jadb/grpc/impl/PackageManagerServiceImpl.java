/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.impl;

import io.grpc.stub.StreamObserver;
import ren.crux.jadb.AndroidDebugBridge;
import ren.crux.jadb.grpc.Jadb;
import ren.crux.jadb.grpc.PackageManagerServiceGrpc;

import static ren.crux.jadb.grpc.Converter.convert;
import static ren.crux.jadb.grpc.Converter.wrap;

/**
 * @author wangzhihui
 */
public class PackageManagerServiceImpl extends PackageManagerServiceGrpc.PackageManagerServiceImplBase {

    private final AndroidDebugBridge adb;

    public PackageManagerServiceImpl() {
        this(new AndroidDebugBridge());
    }

    public PackageManagerServiceImpl(AndroidDebugBridge adb) {
        this.adb = adb;
    }

    @Override
    public void listPackages(Jadb.ListPackagesRequest request, StreamObserver<Jadb.Strings> responseObserver) {
        GrpcResponseHelper.just(req -> {
            Boolean option = null;
            switch (req.getOption()) {
                case system:
                    option = true;
                    break;
                case third_party:
                    option = false;
                    break;
                default:
                    break;
            }
            return convert(adb.shell.pm.listPackages(convert(req.getTarget()), option, req.getFilter()));
        }, request, responseObserver);
    }

    @Override
    public void listPermissionGroups(Jadb.Target request, StreamObserver<Jadb.Strings> responseObserver) {
        GrpcResponseHelper.just(target -> convert(adb.shell.pm.listPermissionGroups(convert(target)))
                , request, responseObserver);
    }

    @Override
    public void listPermissions(Jadb.listPermissionsRequest request, StreamObserver<Jadb.Strings> responseObserver) {
        GrpcResponseHelper.just(req -> convert(adb.shell.pm.listPermissions(convert(req.getTarget()), req.getDangerous()))
                , request, responseObserver);
    }

    @Override
    public void grant(Jadb.PermissionsRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(adb.shell.pm.grant(convert(req.getTarget()), req.getTarget().getPackageName(), req.getPermissions()))
                , request, responseObserver);
    }

    @Override
    public void revoke(Jadb.PermissionsRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(adb.shell.pm.revoke(convert(req.getTarget()), req.getTarget().getPackageName(), req.getPermissions()))
                , request, responseObserver);
    }

    @Override
    public void clear(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(adb.shell.pm.clear(convert(target), target.getPackageName()))
                , request, responseObserver);
    }
}
