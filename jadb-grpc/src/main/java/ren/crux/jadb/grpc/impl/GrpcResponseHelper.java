/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.impl;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;

/**
 * @author wangzhihui
 */
public class GrpcResponseHelper {

    public interface Function<T, R> {

        R apply(T t) throws Exception;
    }

    public static <T, R> void just(Function<T, R> handler, T request, StreamObserver<R> response) {
        try {
            response.onNext(handler.apply(request));
            response.onCompleted();
        } catch (Exception e) {
            response.onError(Status.INTERNAL.withCause(e).asException());
        }
    }
}
