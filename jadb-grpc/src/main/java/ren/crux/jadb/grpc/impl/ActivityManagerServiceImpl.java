/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.impl;

import io.grpc.stub.StreamObserver;
import ren.crux.jadb.AndroidDebugBridge;
import ren.crux.jadb.grpc.ActivityManagerServiceGrpc;
import ren.crux.jadb.grpc.Jadb;

import static ren.crux.jadb.grpc.Converter.convert;
import static ren.crux.jadb.grpc.Converter.wrap;

/**
 * @author wangzhihui
 */
public class ActivityManagerServiceImpl extends ActivityManagerServiceGrpc.ActivityManagerServiceImplBase {

    private final AndroidDebugBridge adb;

    public ActivityManagerServiceImpl() {
        this(new AndroidDebugBridge());
    }

    public ActivityManagerServiceImpl(AndroidDebugBridge adb) {
        this.adb = adb;
    }


    @Override
    public void forceStop(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(
                adb.shell.am.forceStop(convert(target),
                        target.getPackageName())
        ), request, responseObserver);
    }

    @Override
    public void kill(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(
                adb.shell.am.kill(convert(target),
                        target.getPackageName())
        ), request, responseObserver);
    }

    @Override
    public void killAll(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(
                adb.shell.am.killAll(convert(target))
        ), request, responseObserver);
    }

    @Override
    public void start(Jadb.StartActivityRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(
                adb.shell.am.start(convert(req.getTarget()), req.getDebug(), req.getWait(),
                        req.getRepeat(), req.getStop(), convert(req.getTarget().getIntent()))
        ), request, responseObserver);
    }

    @Override
    public void startService(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(
                adb.shell.am.startService(convert(target), convert(target.getIntent()))
        ), request, responseObserver);
    }

    @Override
    public void broadcast(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(
                adb.shell.am.broadcast(convert(target), convert(target.getIntent()))
        ), request, responseObserver);
    }

    @Override
    public void toUri(Jadb.Intent request, StreamObserver<Jadb.Str> responseObserver) {
        GrpcResponseHelper.just(intent -> wrap(
                adb.shell.am.toUri(convert(intent))
        ), request, responseObserver);
    }

    @Override
    public void toIntentUri(Jadb.Intent request, StreamObserver<Jadb.Str> responseObserver) {
        GrpcResponseHelper.just(intent -> wrap(
                adb.shell.am.toIntentUri(convert(intent))
        ), request, responseObserver);
    }
}
