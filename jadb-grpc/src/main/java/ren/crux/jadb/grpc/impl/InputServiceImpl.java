/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.impl;

import io.grpc.stub.StreamObserver;
import ren.crux.jadb.AndroidDebugBridge;
import ren.crux.jadb.grpc.InputServiceGrpc;
import ren.crux.jadb.grpc.Jadb;

import static ren.crux.jadb.grpc.Converter.convert;
import static ren.crux.jadb.grpc.Converter.wrap;

/**
 * @author wangzhihui
 */
public class InputServiceImpl extends InputServiceGrpc.InputServiceImplBase {

    private final AndroidDebugBridge adb;

    public InputServiceImpl() {
        this(new AndroidDebugBridge());
    }

    public InputServiceImpl(AndroidDebugBridge adb) {
        this.adb = adb;
    }

    @Override
    public void keyevent(Jadb.KeyEventRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(
                adb.shell.input.keyevent(convert(req.getTarget()), convert(req.getKey()))
        ), request, responseObserver);
    }

    @Override
    public void text(Jadb.InputTextRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(
                adb.shell.input.text(convert(req.getTarget()), req.getContent())
        ), request, responseObserver);
    }

    @Override
    public void tap(Jadb.TapRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(
                adb.shell.input.tap(convert(req.getTarget()), req.getPoint().getX(), req.getPoint().getY())
        ), request, responseObserver);
    }

    @Override
    public void swipe(Jadb.SwipeRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> {
            Jadb.Point form = req.getTuple().getForm();
            Jadb.Point to = req.getTuple().getForm();
            return wrap(
                    adb.shell.input.swipe(convert(req.getTarget()), form.getX(), form.getY(), to.getX(), to.getY())
            );
        }, request, responseObserver);
    }
}
