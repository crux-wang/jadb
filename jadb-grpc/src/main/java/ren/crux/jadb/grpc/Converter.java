/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc;

import com.google.protobuf.ByteString;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import ren.crux.jadb.model.*;

import java.io.File;
import java.util.*;

/**
 * @author wangzhihui
 */
@Slf4j
public class Converter {

    public static Jadb.Device convert(@NonNull Device device) {
        return Jadb.Device.newBuilder()
                .setSerialNumber(device.getSerialNumber())
                .setDeviceName(device.getDevice())
                .setModel(device.getModel())
                .setProduct(device.getProduct())
                .setUsb(device.getUsb())
                .setEmulator(device.isEmulator())
                .setState(convert(device.getState()))
                .setTransportId(device.getTransportId())
                .build();
    }


    public static Jadb.Devices convertDevices(@NonNull List<Device> devices) {
        Jadb.Devices.Builder builder = Jadb.Devices.newBuilder();
        devices.forEach(device -> builder.addDevices(convert(device)));
        return builder.build();
    }


    public static Jadb.DeviceState convert(@NonNull DeviceState state) {
        return Jadb.DeviceState.valueOf(String.valueOf(state));
    }

    public static Flags convert(@NonNull Jadb.Flags flags) {
        return Flags.valueOf(String.valueOf(flags));
    }

    public static List<Flags> convertFlags(@NonNull List<Jadb.Flags> jflags) {
        List<Flags> flags = new ArrayList<>();
        jflags.forEach(f -> flags.add(convert(f)));
        return flags;
    }

    public static KeyEvent convert(@NonNull Jadb.KeyEvent keyEvent) {
        return KeyEvent.valueOf(String.valueOf(keyEvent));
    }

    public static Target convert(@NonNull Jadb.Target target) {
        return new Target(convert(target.getType()), target.getSerialNumber());
    }

    public static TargetType convert(@NonNull Jadb.TargetType targetType) {
        return TargetType.valueOf(String.valueOf(targetType));
    }

    public static Intent convert(@NonNull Jadb.Intent jintent) {
        Intent intent = new Intent();
        intent.setAction(jintent.getAction());
        intent.setComponent(jintent.getComponent());
        intent.setCategory(jintent.getCategory());
        intent.setMimeType(jintent.getMimeType());
        intent.setFlagsValue(jintent.getFlagsValue());
        intent.setDataUri(jintent.getDataUri());
        intent.setSelector(jintent.getSelector());
        intent.setExtras(convert(jintent.getExtrasMap()));
        intent.setFlags(convertFlags(jintent.getFlagsList()));
        return intent;
    }


    public static Map<String, Object> convert(Map<String, Jadb.IntentExtraValue> jextra) {
        Map<String, Object> extra = new HashMap<>(16);
        jextra.forEach((k, v) -> {
            switch (v.getType()) {
                case _string:
                    extra.put(k, v.getStringVal());
                    break;
                case _int:
                    extra.put(k, v.getIntVal());
                    break;
                case _long:
                    extra.put(k, v.getLongVal());
                    break;
                case _float:
                    extra.put(k, v.getFloatVal());
                    break;
                case _bool:
                    extra.put(k, v.getBoolVal());
                    break;
                case _int_array:
                    extra.put(k, v.getIntArrayValList().toArray());
                    break;
                case _long_array:
                    extra.put(k, v.getLongArrayValList().toArray());
                    break;
                case _float_array:
                    extra.put(k, v.getFloatArrayValList().toArray());
                    break;
                default:
                    break;
            }
        });
        return extra;
    }

    public static Optional<File> convert(@NonNull Jadb.FileTuple fileTuple) {
        try {
            File apk = File.createTempFile(fileTuple.getName(), "apk");
            ByteString bytes = fileTuple.getBytes();
            FileUtils.writeByteArrayToFile(apk, bytes.toByteArray());
            return Optional.of(apk);
        } catch (Exception e) {
            log.error("Convert FileTuple ( {} ) error : ", fileTuple, e);
        }
        return Optional.empty();
    }

    public static Jadb.Bool wrap(boolean bool) {
        return Jadb.Bool.newBuilder().setSuccess(bool).build();
    }

    public static Jadb.Str wrap(String string) {
        return Jadb.Str.newBuilder().setString(string).build();
    }

    public static Jadb.Strings convert(List<String> strings) {
        return Jadb.Strings.newBuilder().addAllList(strings).build();
    }

}
