/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import ren.crux.jadb.grpc.ActivityManagerServiceGrpc;
import ren.crux.jadb.grpc.AndroidDebugBridgeServiceGrpc;
import ren.crux.jadb.grpc.InputServiceGrpc;
import ren.crux.jadb.grpc.PackageManagerServiceGrpc;

import java.util.concurrent.TimeUnit;

/**
 * @author wangzhihui
 */
@Slf4j
public class AndroidDebugBridgeClient {

    private final ManagedChannel channel;
    public final AndroidDebugBridgeServiceGrpc.AndroidDebugBridgeServiceBlockingStub adb;
    public final ActivityManagerServiceGrpc.ActivityManagerServiceBlockingStub am;
    public final PackageManagerServiceGrpc.PackageManagerServiceBlockingStub pm;
    public final InputServiceGrpc.InputServiceBlockingStub input;

    /**
     * Construct client connecting to HelloWorld server at {@code host:port}.
     */
    public AndroidDebugBridgeClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
    }

    /**
     * Construct client for accessing HelloWorld server using the existing channel.
     */
    private AndroidDebugBridgeClient(ManagedChannel channel) {
        this.channel = channel;
        adb = AndroidDebugBridgeServiceGrpc.newBlockingStub(channel);
        am = ActivityManagerServiceGrpc.newBlockingStub(channel);
        pm = PackageManagerServiceGrpc.newBlockingStub(channel);
        input = InputServiceGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

}
