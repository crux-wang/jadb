/*
 *
 *    Copyright 2018 The Crux Authors
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ren.crux.jadb.grpc.impl;

import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ren.crux.jadb.AndroidDebugBridge;
import ren.crux.jadb.grpc.AndroidDebugBridgeServiceGrpc;
import ren.crux.jadb.grpc.Jadb;

import java.io.File;
import java.util.Optional;

import static ren.crux.jadb.grpc.Converter.*;

/**
 * @author wangzhihui
 */
@Slf4j
public class AndroidDebugBridgeServiceImpl extends AndroidDebugBridgeServiceGrpc.AndroidDebugBridgeServiceImplBase {

    private final AndroidDebugBridge adb;

    public AndroidDebugBridgeServiceImpl() {
        this.adb = new AndroidDebugBridge();
    }

    public AndroidDebugBridgeServiceImpl(@NonNull AndroidDebugBridge adb) {
        this.adb = adb;
    }

    @Override
    public void devices(Jadb.Void request, StreamObserver<Jadb.Devices> responseObserver) {
        GrpcResponseHelper.just(req -> convertDevices(adb.devices()), request, responseObserver);
    }

    @Override
    public void tcpip(Jadb.Port request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(adb.tcpip(req.getPort())), request, responseObserver);
    }

    @Override
    public void connect(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(adb.connect(target.getSerialNumber())), request, responseObserver);
    }

    @Override
    public void disconnect(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(adb.disconnect(target.getSerialNumber())), request, responseObserver);
    }

    @Override
    public void usb(Jadb.Target request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> wrap(adb.usb(convert(target))), request, responseObserver);
    }

    @Override
    public void install(Jadb.InstallRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(target -> {
            Optional<File> optional = convert(request.getApk());
            if (!optional.isPresent()) {
                throw new Exception("Invalid Apk File.");
            }
            return wrap(
                    adb.install(convert(request.getTarget()),
                            optional.get().getCanonicalPath(),
                            request.getGrantAllPermissions(),
                            request.getReplace())
            );
        }, request, responseObserver);
    }

    @Override
    public void uninstall(Jadb.UninstallRequest request, StreamObserver<Jadb.Bool> responseObserver) {
        GrpcResponseHelper.just(req -> wrap(
                adb.uninstall(convert(req.getTarget()),
                        req.getTarget().getPackageName(),
                        req.getKeep())
        ), request, responseObserver);
    }
}
